# ¿Qué es Git?

Es una herramienta que realiza una función del control de versiones de código de forma distribuida ,nos va a servir para trabajar en equipo de una manera mucho más simple y optima cuando estamos desarrollando software, de forma que si tenemos dos o tres personas trabajando en ciertas funcionalidades del proyecto y nosotros podemos estar trabajando en nuestra parte del código. Con Git vamos a poder controlar todos los cambios que se hacen en nuestra aplicación y en nuestro código y vamos a tener control absoluto de todo lo que pasa en el código, pudiendo volver atrás en el tiempo, pudiendo abrir diferentes ramas de desarrollo. Cuando acabamos de desarrollar nuestro código, utilizamos Git para mezclar los cambios con los otros compañeros. De forma que el código se mezcla de manera perfecta sin generar ningún tipo de fallo y de forma rápida.
También nos va a proporcionar un listado de los cambios *(commits)* y podemos volver atrás en el tiempo a cualquiera de esos cambios o commits.
Además tendremos la posibilidad de trabajar con ramas de desarrollo, que nos van a permitir desarrollar cosas que divergen mucho del programa principal. Estas son las cosas más importantes que nos ofrece Git, es una herramienta imprescindible para cualquier desarrollador en la actualidad.

# ¿Qué es GitLab?

Es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto. GitLab es una suite completa que permite gestionar, administrar, crear y conectar los repositorios con diferentes aplicaciones y hacer todo tipo de integraciones con ellas, ofreciendo un ambiente y una plataforma en cual se puede realizar las varias etapas de su **SDLC/ADLC** y **DevOps**.
Fue escrito por los programadores ucranianos *Dmitriy Zaporozhets* y *Valery Sizov* en el lenguaje de programación **Ruby1** con algunas partes reescritas posteriormente en **Go**, inicialmente como una solución de gestión de código fuente para colaborar con su equipo en el desarrollo de software. Luego evolucionó a una solución integrada que cubre el ciclo de vida del desarrollo de software, y luego a todo el ciclo de vida de **DevOps**. La arquitectura tecnológica actual incluye **Go**, **Ruby on Rails** y **Vue.js**.
La compañía, GitLab Inc. cuenta con un equipo de 1309 miembros. Es usado por organizaciones como la NASA, el CERN, IBM y/o Sony.

# ¿Qué es GitHub?

Es una plataforma de desarrollo colaborativo para alojar proyectos utilizando el sistema de control de versiones Git. Se utiliza principalmente para la creación de código fuente de programas de ordenador. El software que opera GitHub fue escrito en **Ruby on Rails**. Desde enero de 2010, GitHub opera bajo el nombre de GitHub, Inc. Anteriormente era conocida como Logical Awesome LLC. El código de los proyectos alojados en GitHub se almacena típicamente de forma pública.
El 4 de junio de 2018 Microsoft compró GitHub por la cantidad de 7500 millones de dólares, un cambio de propietarios que ha provocado la salida de varios proyectos de este repositorio debido la posibilidad de acceso a códigos fuentes por parte de una compañía que basa uno de sus principales negocios en el software.

# ¿Qué es Markdown?

Es un lenguaje de marcado ligero creado por John Gruber que trata de conseguir la máxima legibilidad y facilidad de publicación tanto en su forma de entrada como de salida, inspirándose en muchas convenciones existentes para marcar mensajes de correo electrónico usando texto plano. Se distribuye bajo licencia BSD y se distribuye como plugin (o al menos está disponible) en diferentes sistemas de gestión de contenidos **(CMS)**. Markdown convierte el texto marcado en documentos *XHTML* utilizando html2text creado por Aaron Swartz. Markdown fue implementado originariamente en Perl por Gruber, pero desde entonces ha sido traducido a multitud de lenguajes de programación, incluyendo **PHP**, **Python**, **Ruby**, **Java** y **Common Lisp**.

# Comandos básicos de GIT

•	**git config**:
Uno de los comandos más usados en git es git config, que puede ser usado para establecer una configuración específica de usuario, como sería el caso del email, un algoritmo preferido para diff, nombre de usuario y tipo de formato, etc… Por ejemplo, el siguiente comando se usa para establecer un email:

     git config --global user.email sam@google.com

•	**git init**:
Este comando se usa para crear un nuevo repertorio GIT:

     git init

•	**git add**
Este comando puede ser usado para agregar archivos al index. Por ejemplo, el siguiente comando agrega un nombre de archivo temp.txt en el directorio local del index:

     git add temp.txt

•	**git clone**
Este comando se usa con el propósito de revisar repertorios. Si el repertorio está en un servidor remoto se tiene que usar el siguiente comando:

     git clone alex@93.188.160.58:/path/to/repository

Pero si estás por crear una copia local funcional del repertorio, usa el comando:

     git clone /path/to/repository

•	**git commit**
El comando commit es usado para cambiar a la cabecera. Ten en cuenta que cualquier cambio comprometido no afectara al repertorio remoto. Usa el comando:

     git commit –m “Message to go with the commit here”

•	**git status**
Este comando muestra la lista de los archivos que se han cambiado junto con los archivos que están por ser añadidos o comprometidos.

     git status

•	**git push**
Este es uno de los comandos más básicos. Un simple push envía los cambios que se han hecho en la rama principal de los repertorios remotos que están asociados con el directorio que está trabajando. Por ejemplo:

     git push  origin master

•	**git checkout**
El comando checkout se puede usar para crear ramas o cambiar entre ellas. Por ejemplo, el siguiente comando crea una nueva y se cambia a ella:

     command git checkout -b <banch-name>

Para cambiar de una rama a otra solo usa:

     git checkout <branch-name>

•	**git remote**
El comando git se usa para conectar a un repositorio remoto. El siguiente comando muestra los repositorios remotos que están configurados actualmente:

     git remote -v

Este comando te permite conectar al usuario con el repositorio local a un servidor remoto:

     git remote add origin <93.188.160.58>

•	**git branch**
Este comando se usa para listar, crear o borrar ramas. Para listar todas las ramas se usa:

     git branch

para borrar la rama:

     git branch -d <branch-name>

•	**git pull**
Para poder fusionar todos los cambios que se han hecho en el repositorio local trabajando, el comando que se usa es:

     git pull

•	**git merge**
Este comando se usa para fusionar una rama con otra rama activa:

     git merge <branch-name>

•	**git diff**
Este comando se usa para hacer una lista de conflictos. Para poder ver conflictos con el archivo base usa

     git diff

•	**git tag**
Etiquetar se usa para marcar commits específicos con asas simples. Por ejemplo:

     git tag 1.1.0 <instert-commitID-here>
•	**git log**
Ejecutar este comando muestra una lista de commits en una rama junto con todos los detalles. Por ejemplo:

     commit 15f4b6c44b3c8344caasdac9e4be13246e21sadw

Author: Alex Hunter <alexh@gmail.com>
Date:   Mon Oct 1 12:56:29 2016 -0600

•	**git reset**
Para resetear el index y el directorio que está trabajando al último estado comprometido se usa este comando:

     git reset - -hard HEAD

•	**git rm**
Este comando se puede usar para remover archivos del index y del directorio que está trabajando:

     git rm filename.txt

•	**git stash**
Este es uno de los comandos menos conocidos, pero ayuda a salvar cambios que no están por ser comprometidos inmediatamente, pero temporalmente:

     git stash

•	**git show**
Se usa para mostrar información sobre cualquier objeto git. Por ejemplo:

     git show

•	**git fetch**
Este comando le permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local que está trabajando. Por ejemplo:

     git fetch origin

•	**git ls-tree**
Para ver un objeto de árbol junto con el nombre y modo de cada uno de ellos, y el valor blob´s SHA-1, se usa:

     git ls-tree HEAD

•	**git cat-file**
Usando el valor SHA-1, se puede ver el tipo de objeto usando este comando. Por ejemplo:

     git cat-file –p d670460b4b4aece5915caf5c68d12f560a9fe3e4

•	**git grep**
Este comando le permite al usuario buscar en los árboles de contenido cualquier frase o palabra. Por ejemplo, para buscar por www.tupaginaweb.com en todos los archivos se usaría:

     git grep “www.tupaginaweb.com”

•	**gitk**
Este es la interfaz gráfica para un repositorio local que puede invocar escribiendo y ejecutando:

     gitk

•	**git instaweb**
Con este comando un servidor web puede correr interconectado con el repositorio local. Un navegador web también está automáticamente dirigido a el:

     git instaweb –http=webrick

•	**git gc**
Para optimizar el repositorio por medio de una recolección de basura, que limpiara archivos innecesarios y los optimizara, usa:

     git gc

•	**git archive**
Este comando le permite al usuario crear archivos zip o tar que contengan los constituyentes de un solo árbol de repositorio:

     git archive – -format=tar master

•	**git prune**
Con este comando los objetos que no tengan ningún puntero entrante serán eliminados:

     git prune

•	**git fsck**
Para poder hacer un chequeo de integridad del sistema de archivos git, usa este comando. Cualquier objeto corrompido será detectado:

     git fsck

•	**git rebase**
Este comando se usa para la re aplicación de los compromisos en otra rama. Por ejemplo:

     git rebase master



# ¿Qué son las ramas?

En Git las Ramas son espacios o entornos independientes para que un Desarrollador sea Back-end, Front-end, Tester, etc. pueda usar y así trabajar sobre un mismo Proyecto sin chancar o borrar el conjunto de archivos originales del proyecto, dándonos flexibilidad para desarrollar nuestro proyecto de manera mas organizada.
Cuando hablamos de ramificaciones, significa se ha tomado la rama principal de desarrollo (master) y a partir de ahí has continuado trabajando sin seguir la rama principal de desarrollo. En muchos sistemas de control de versiones este proceso es costoso, pues a menudo requiere crear una nueva copia del código, lo cual puede tomar mucho tiempo cuando se trata de proyectos grandes.
Algunas personas resaltan que uno de los puntos más fuertes de Git es su sistema de ramificaciones y lo cierto es que esto le hace resaltar sobre los otros sistemas de control de versiones. ¿Por qué esto es tan importante? La forma en la que Git maneja las ramificaciones es increíblemente rápida, haciendo así de las operaciones de ramificación algo casi instantáneo, al igual que el avance o el retroceso entre distintas ramas, lo cual también es tremendamente rápido. A diferencia de otros sistemas de control de versiones, Git promueve un ciclo de desarrollo donde las ramas se crean y se unen ramas entre sí, incluso varias veces en el mismo día. Entender y manejar esta opción te proporciona una poderosa y exclusiva herramienta que puede, literalmente, cambiar la forma en la que desarrollas.

# ¿Cuando usar git init?

Este un comando que se utiliza una sola vez y sirve para crear un repositorio nuevo, creando un subdirectorio git en tu directorio de trabajo actual. También creara una nueva rama maestra.

# ¿Para qué se usa git clone?

Este comando se emplea para copiar el repositorio localmente, es decir, que copiamos la carpeta ( repositorio) que se encuenra en gitlab a nuestra pc.

# ¿Cuando usar git push?

Este comando se usa cuando vamos a subir los cambios que hemos hecho en nuestra carpeta local o pc  a nuestro repositorio en línea de gitlab.

# ¿Diferencias entre git push y git pull?

El comando ***git push*** se utiliza para subir los cambios del repositorio, desde la pc al repositorio en línea, en cambio ***git pull*** se emplea para descargar o actualizar el repositorio en línea con el de la pc, esto se hace cuando hay cambios en el repositorio en línea generalmente por colaboradores del proyecto.

# ¿Cuando usar git add?

Este comando se utiliza para dar conocimiento a git que se ha empleado un cambio a la carpeta local, es el primer paso para subir los cambios al repositorio en línea.

# ¿Cual es la finalidad de un git commit?

La finalidad de este comando es registrar los cambios que ya conoce git, este se emplea después del git add.

# Cómo funciona el flujo de trabajo en GIT.

Cuando evalúas un flujo de trabajo para tu equipo, lo más importante es que tengas en cuenta la cultura de tu equipo. La idea es que el flujo de trabajo mejore la eficacia de tu equipo, no que sea una carga que limite la productividad.

El flujo de trabajo centralizado consiste en un bloque de construcción de otros flujos de trabajo de Git. Los flujos de trabajo de Git más populares cuentan con una especie de repositorio centralizado del que los desarrolladores pueden incorporar cambios y al que pueden enviar los suyos.

## Flujo de trabajo centralizado

El flujo de trabajo centralizado es un flujo de trabajo de Git estupendo para equipos que están realizando la migración desde SVN. Al igual que Subversion, el flujo de trabajo centralizado usa un repositorio central como punto de entrada de todos los cambios al proyecto. En lugar de trunk, la rama de desarrollo predeterminada se llama master, y todos los cambios se confirman en dicha rama. Este flujo de trabajo no requiere más ramas que master.
La migración a un sistema de control de versiones distribuido puede parecer una tarea complicada, pero no tienes que cambiar tu flujo de trabajo existente para sacarle partido a Git. Tu equipo puede desarrollar proyectos del mismo modo en el que lo hacían con Subversion.

No obstante, usar Git para impulsar tu flujo de trabajo de desarrollo presenta algunas ventajas en comparación con SVN. En primer lugar, le brinda a cada desarrollador su propia copia local del proyecto completo. Este entorno aislado deja que cada desarrollador trabaje de forma independiente a todos los demás cambios de un proyecto; pueden añadir confirmaciones a su repositorio local y olvidarse por completo de los desarrollos posteriores hasta que los necesiten.
En segundo lugar, te proporciona acceso al sólido modelo de fusión y creación de ramas de Git. Al contrario que SVN, las ramas de Git se han diseñado para constituir un mecanismo de seguridad de integración de código y uso compartido de cambios entre repositorios. El flujo de trabajo centralizado es similar a otros flujos de trabajo en lo que respecta al uso de un repositorio remoto alojado en servidor al que los desarrolladores pueden realizar envíos e incorporar cambios. En comparación con otros flujos de trabajo, el flujo de trabajo centralizado no tiene solicitudes de incorporación de cambios o patrones de bifurcación. Un flujo de trabajo centralizado suele ser más adecuado para equipos que migran desde SVN a Git y otros de tamaño reducido.

## Funcionamiento

Los desarrolladores empiezan por clonar el repositorio central. En sus propias copias locales del proyecto, editan archivos y confirman cambios como lo harían en SVN; no obstante, estas nuevas confirmaciones se almacenan de forma local, completamente aisladas del repositorio central. Esto permite que los desarrolladores aplacen la sincronización con los niveles superiores hasta que realicen una pausa.
Para publicar cambios en el proyecto oficial, los desarrolladores "envían" su rama master local al repositorio central. Este proceso es equivalente a svn commit, excepto porque añade todas las confirmaciones locales que no se han enviado todavía a una rama master.

## Inicialización del repositorio central

En primer lugar, hace falta crear el repositorio central en un servidor. Si se trata de un proyecto nuevo, puedes inicializar un repositorio vacío. Si no es así, tendrás que importar un repositorio Git o SVN existente.
Los repositorios centrales deben ser siempre bare (vacíos, es decir, sin ningún directorio en funcionamiento), que se pueden crear de esta forma:
ssh user@host git init --bare /path/to/repo.git
Asegúrate de utilizar un nombre de usuario de SSH válido en user, el dominio o la dirección IP de tu servidor en host y la ubicación en la que quieres almacenar el repositorio en /path/to/repo.git. Ten en cuenta que la extensión .git se sueñe añadir al nombre del repositorio para indicar que es un repositorio bare (vacío).

## Repositorios centrales alojados

Los repositorios centrales se suelen crear mediante servicios de alojamiento de Git de terceros, como Bitbucket Cloud o Bitbucket Server. El servicio de alojamiento realiza el proceso de inicializar un repositorio bare (vacío) que se ha explicado anteriormente en tu lugar. A continuación, dicho servicio de alojamiento proporcionará una dirección para acceder al repositorio central desde tu repositorio local.

## Clonación del repositorio central

El siguiente desarrollador crea una copia local de todo el proyecto. Esto se consigue mediante el comando git clone:
git clone ssh://user@host/path/to/repo.git
Cuando clonas un repositorio, Git añade automáticamente un atajo de teclado llamado origin que apunta al repositorio principal, ya que asume que querrás interactuar con él más adelante. 

## Aplicación de cambios y confirmaciones

Cuando el repositorio se clona de forma local, el desarrollador puede aplicar cambios mediante el proceso de confirmación estándar de Git: editar, preparar y confirmar. Si no te has familiarizado con el entorno de ensayo, debes saber que consiste en una forma de preparar una confirmación sin tener que incluir todos los cambios del directorio en funcionamiento. Esto te permite crear confirmaciones con un enfoque muy claro, incluso si has aplicado muchos cambios locales.

     git status # Muestra el estado del repositorio
     git add <some-file> # Prepara un archivo
     git commit # Confirma un archivo</some-file>

Recuerda que, como estos comandos crean confirmaciones locales, John puede repetir el proceso todas las veces que quiera sin tener que preocuparse por lo que esté sucediendo en el repositorio central. Esta característica puede resultar muy útil para funciones de gran tamaño que se tienen que desglosar en bloques más sencillos y reducidos.

## Envío de nuevas confirmaciones al repositorio central

Una vez que se hayan aplicado cambios nuevos al repositorio local, se deberán enviar estos cambios para compartirlos con otros desarrolladores del proyecto.

     git push origin master

Este comando enviará los nuevos cambios confirmados al repositorio central. Cuando envías cambios al repositorio central, es posible que las actualizaciones enviadas anteriormente por otros desarrolladores contengan código que entra en conflicto con las actualizaciones que tú quieres enviar. Git mostrará un mensaje en el que se indica este problema. En esta situación, tendrá que ejecutarse el comando git pull en primer lugar. Esta situación de conflicto se explica en profundidad en la sección siguiente.

## Gestión de conflictos

El repositorio central representa el proyecto oficial, por lo que su historial de confirmación se debe tratar como algo sagrado e inmutable. Si las confirmaciones locales de un desarrollador difieren de las del repositorio central, Git rechazará el envío de cambios porque estos sobrescribirían las confirmaciones oficiales.

Antes de que el desarrollador pueda publicar una función, tiene que buscar y actualizar las confirmaciones centrales y reorganizar los cambios a partir de ellas. Es como decir "quiero añadir mis cambios a lo que los demás ya han hecho". El resultado es un historial perfectamente lineal, como las de los flujos de trabajo de SVN tradicionales.

Si los cambios locales entran en conflicto directo con las confirmaciones en niveles superiores, Git pondrá en pausa el proceso de modificación de bases y te dará la oportunidad de resolver los conflictos de forma manual. Lo bueno de Git es que usa los mismos comandos, git status y git add, tanto para generar confirmaciones como para resolver conflictos de fusión. Esto hace que a los nuevos desarrolladores les resulte sencillo gestionar sus propias fusiones. Además, si tienen problemas, Git hace que sea muy fácil abandonar por completo la reorganización y volver a intentarlo (o ir a pedir ayuda).

# Ejemplo:

Dos desarrolladores, John y Mary, pueden trabajar en funciones separadas y compartir sus contribuciones mediante un repositorio centralizado.

## John trabaja en su función
 
En este repositorio local, John puede desarrollar funciones mediante el proceso de confirmación estándar de Git: editar, preparar y confirmar.
Recuerda que, como estos comandos crean confirmaciones locales, John puede repetir este proceso tantas veces como desee sin tener que preocuparse por lo que está pasando en el repositorio central.

## Mary trabaja en su función
 
Mientras tanto, Mary trabaja en su propia función y en su propio repositorio local con el mismo proceso de cambio, preparación y confirmación. Al igual que a John, no le importa lo que esté sucediendo en el repositorio central y, por supuesto, tampoco le importa lo que John está haciendo en su repositorio local, ya que todos los repositorios locales son privados.

## John publica su función
 
Una vez que John ha completado su función, debe publicar sus confirmaciones locales en el repositorio central, de forma que otros miembros del equipo puedan acceder a ellas. Puede hacerlo con el comando git push de este modo:

     git push origin master

Recuerda que origin es la conexión remota al repositorio central que Git creó cuando John lo clonó. El argumento master le indica a Git que intente hacer que la rama master de origin tenga el mismo aspecto que su rama master local. Como el repositorio central no se ha actualizado desde que John lo clonó, esta acción no creará ningún conflicto y el envío se realizará según lo previsto.

## Mary intenta publicar su función
 
Veamos lo que pasa cuando Mary intenta enviar su función después de que John haya publicado con éxito sus cambios en el repositorio central. Puede usar el mismo comando push:

     git push origin master

 No obstante, como su historial local es distinto que el del repositorio, git rechazará la solicitud con un mensaje de error algo extenso:

      error: failed to push some refs to '/path/to/repo.git'
     hint: Updates were rejected because the tip of your current branch is behind
     hint: its remote counterpart. Merge the remote changes (e.g. 'git pull')
     hint: before pushing again.
     hint: See the 'Note about fast-forwards' in 'git push --help' for details.

Así se evita que Mary sobrescriba las confirmaciones oficiales. Tiene que incorporar los cambios de las actualizaciones de John a su repositorio, integrarlos con sus cambios locales y, a continuación, volver a intentarlo.

## Mary realiza una reorganización a partir de las confirmaciones de John
 
Mary puede usar el comando git pull para incorporar cambios del nivel superior a su repositorio. Este comando es parecido a svn update, ya que incorpora los cambios de todo el historial de confirmación del nivel superior al repositorio local de Mary e intenta integrarlos con sus confirmaciones locales:

     git pull --rebase origin master

La opción --rebase le indica a Git que transfiera todas las confirmaciones de Mary al extremo de la rama master después de sincronizarlos con el repositorio central, como se muestra a continuación:
 
La incorporación de cambios seguirá funcionando si se te olvida esta opción, pero es posible que tengas que realizar una confirmación de fusión cada vez que alguien tenga que realizar una sincronización con el repositorio central. Para este flujo de trabajo, siempre es mejor reorganizar en lugar de generar una confirmación de fusión.

## Mary soluciona un conflicto de fusión
 
Modifica las bases de los trabajos mediante la transferencia de cada confirmación local a la rama master actualizada de una en una. De este modo, descubrirás los conflictos de fusión de cada confirmación, en lugar de tener que resolverlos todos con una confirmación de fusión masiva. Además, esto hace que tus confirmaciones mantengan un enfoque lo más claro posible, y produce un historial del proyecto limpio. También hace que resulte mucho más sencillo descubrir qué errores se han introducido y, si fuera necesario, revertir cambios con un impacto mínimo sobre el proyecto.

Si Mary y John trabajan en funciones que no están relacionadas, es poco probable que el proceso de reorganización genere conflictos. No obstante, si así fuera, Git pausará la reorganización en la confirmación actual y mostrará el siguiente mensaje, además de algunas instrucciones importantes:

     CONFLICT (content): Merge conflict in <some-file>

Lo bueno de Git es que cualquiera puede resolver sus propios conflictos de fusión. En nuestro ejemplo, Mary solo tiene que ejecutar un comando git status para ver dónde está el problema. Los archivos en conflicto se mostrarán en la sección Rutas no fusionadas:

     # (usa "git reset HEAD <some-file>..." para deshacer la reparación)#
     (usa "git add/rm <some-file>..." según corresponda para indicar la resolución)
     #
     # ambos modificados: <some-file>

A continuación, editará los archivos como considere. Una vez que esté contenta con el resultado, podrá preparar los archivos según el método habitual y dejar que git rebase haga el resto:

     git add <some-file>
     git rebase --continue

Y esto es todo lo que hay que hacer. Git continuará con la siguiente confirmación y repetirá el proceso con cualquier otra confirmación que genere conflictos.
Si llegas a este punto y te das cuenta de que no tienes ni idea de lo que está pasando, no entres en pánico. Solo tienes que ejecutar el siguiente comando y volverás al punto de inicio:

     git rebase --abort

## Mary publica correctamente su función
 
Después de terminar con la sincronización del repositorio central, Mary podrá publicar los cambios correctamente:

     git push origin master

## Hacia dónde ir

Como se puede ver, es posible replicar un entorno de desarrollo de Subversion tradicional con solo unos comandos de Git. Este método es perfecto para los equipos que están migrando de SVN, pero no le sacan partido a la naturaleza distribuida de Git.
El flujo de trabajo centralizado es estupendo para equipos pequeños. El proceso de resolución de conflictos explicado anteriormente puede provocar un cuello de botella conforme tu equipo escala su tamaño. Si tu equipo está contento con el flujo de trabajo centralizado, pero quiere perfeccionar la colaboración, merece la pena analizar las ventajas del flujo de trabajo de rama de función. Al dedicar una rama aislada a cada función, es posible iniciar conversaciones en profundidad acerca de nuevas adiciones antes de integrarlas en el proyecto oficial.




